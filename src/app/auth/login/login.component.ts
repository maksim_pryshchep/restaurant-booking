import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { State } from '../../state/app.state';
import * as fromAuth from '../state/auth.actions';
import { lastError } from '../state/auth.selectors';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { ErrorObject } from '../state/auth.reducer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  passwordVisible = false;
  loginForm: FormGroup;
  loginError = false;
  authError: Subscription;

  constructor(private store: Store<State>,
              private fb: FormBuilder) {
    this.authError = this.store.pipe(select(lastError),
      filter((x: ErrorObject) => x && (x.code === 'auth/wrong-password' || x.code === 'auth/user-not-found')))
      .subscribe(
        res => {
          this.loginError = true;
          this.loginForm.reset();
          this.validateForm();
        }
      )
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
    });
  }

  ngOnDestroy() {
    this.authError.unsubscribe();
  }

  signIn() {
    this.loginError = false;
    if (!this.loginForm.valid) {
      this.validateForm();
    } else {
      this.store.dispatch(new fromAuth.SignIn({
        login: this.loginForm.controls.email.value,
        password: this.loginForm.controls.password.value
      }));
    }
  }

  validateForm() {
    for (const i in this.loginForm.controls) {
      this.loginForm.controls[i].markAsDirty();
      this.loginForm.controls[i].updateValueAndValidity();
      this.loginForm.controls[i].markAsTouched();
    }
  }
}
