import {Action} from '@ngrx/store';
import {User, SignUpOptions} from '../model/models';

import {User as FirebaseUser} from 'firebase';

export enum AuthActionTypes {
  SignUp = '[Auth] Create New User',
  GetUser = '[Auth] Get User',
  Authenticated = '[Auth] Authenticated',
  NotAuthenticated = '[Auth] Not Authenticated',
  SignIn = '[Auth] Sign In',
  SocialNetworkSignIn = '[Auth] Social Network Sign In',
  SignInCompleted = '[Auth] Sign In Completed',
  SignOut = '[Auth] Sign Out',
  AuthOperationFailed = '[Auth] Auth Operation Failed',
  CheckEmailVerified = '[Auth] Check Email Verified',
  SaveUserData = '[Auth] Save User Data',
  InitUserDataAfterSignUp = '[Auth] Init User Data After Sign Up',
  SendVerificationMail = '[Auth] Send Verification Email'
}

export class SignInCompleted implements Action {
  readonly type = AuthActionTypes.SignInCompleted;
}

export class SaveUserData implements Action {
  readonly type = AuthActionTypes.SaveUserData;

  constructor(public payload: User) {

  }
}

export class CheckEmailVerified implements Action {
  readonly type = AuthActionTypes.CheckEmailVerified;

  constructor(public payload: {
    uid: string,
    emailVerified: boolean
  }) {
  }
}

export class GetUser implements Action {
  readonly type = AuthActionTypes.GetUser;
}

export class Authenticated implements Action {
  readonly type = AuthActionTypes.Authenticated;

  constructor(public payload: { uid: string }) {

  }
}

export class NotAuthenticated implements Action {
  readonly type = AuthActionTypes.NotAuthenticated;
}

export class CreateNewUser implements Action {
  readonly type = AuthActionTypes.SignUp;

  constructor() {
  }
}

export class SignIn implements Action {
  readonly type = AuthActionTypes.SignIn;

  constructor(public payload: { login: string; password: string }) {
  }
}

export class SignOut implements Action {
  readonly type = AuthActionTypes.SignOut;
}

export class AuthOperationFailed implements Action {
  readonly type = AuthActionTypes.AuthOperationFailed;

  constructor(public payload: any) {
  }
}

export class InitUserDataAfterSignUp implements Action {
  readonly type = AuthActionTypes.InitUserDataAfterSignUp;

  constructor(public payload: { user: FirebaseUser; options: SignUpOptions }) {

  }
}

export class SignUp implements Action {
  readonly type = AuthActionTypes.SignUp;

  constructor(public payload: { email: string; password: string; firstName: string; lastName: string, phoneNumber: string }) {

  }
}

export class SendVerificationMail implements Action {
  readonly type = AuthActionTypes.SendVerificationMail;
}

export class SocialNetworkSignIn implements Action {
  readonly type = AuthActionTypes.SocialNetworkSignIn;

  constructor(public payload: { type: string }) {

  }
}

export type AuthActions =
  SocialNetworkSignIn
  | InitUserDataAfterSignUp
  | SignInCompleted
  | SaveUserData
  | CreateNewUser
  | SignIn
  | SignOut
  | AuthOperationFailed
  | GetUser
  | Authenticated
  | NotAuthenticated
  | CheckEmailVerified;
