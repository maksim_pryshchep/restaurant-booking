import {createSelector, createFeatureSelector} from '@ngrx/store';
import {AuthState} from './auth.reducer';
import { withLatestFrom } from 'rxjs/operators';
import { getCurrentRestaurantProfile } from '../../restaurants/state/restaurants.selectors';
import { User } from '../model/models';
import { Restaurant } from '../../restaurants/model/models';

const getAuthFeatureState = createFeatureSelector<AuthState>('auth');

export const lastError = createSelector(
  getAuthFeatureState,
  state => state.error
);

export const isAuthenticated = createSelector(
  getAuthFeatureState,
  state => !!state.user
);

export const getDisplayNameValue = createSelector(
  getAuthFeatureState,
  state => state.user && (state.user.firstName || state.user.displayName)
);

export const isLoadingUser = createSelector(
  getAuthFeatureState,
  state => state.loading
);

export const getCurrentUser = createSelector(
  getAuthFeatureState,
  state => state.user
);

export const isFavoriteRestaurant = createSelector(
  getCurrentUser,
  getCurrentRestaurantProfile,
  (currentUser: User, restaurant: Restaurant) => {
    if (currentUser && restaurant) {
      return restaurant.inFavorites.includes(currentUser.uid);
    } else {
      return false;
    }
  }
);
