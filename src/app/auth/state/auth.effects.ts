import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import * as authActions from './auth.actions';
import {switchMap, catchError, map, tap, mergeMap, flatMap, filter} from 'rxjs/operators';
import {User} from '../model/models';
import {AngularFireAuth} from '@angular/fire/auth';
import {of, from} from 'rxjs';
import {User as FirebaseUser, auth} from 'firebase';
import {AngularFirestore} from '@angular/fire/firestore';
import {Router} from '@angular/router';

export const toPayload = <T>(action: { payload: T }) => action.payload;

@Injectable() export class AuthEffects {
  constructor(private actions$: Actions,
              private afAuth: AngularFireAuth,
              private afs: AngularFirestore,
              private router: Router) {

  }

  @Effect() getUser$ = this.actions$.pipe(
    ofType<authActions.GetUser>(authActions.AuthActionTypes.GetUser),
    switchMap(x => this.afAuth.authState),
    map((authData: FirebaseUser) => {
      if (authData) {
        return new authActions.Authenticated({
          uid: authData.uid
        });
      } else {
        return new authActions.NotAuthenticated();
      }
    }),
    catchError(err => of(new authActions.NotAuthenticated()))
  );

  @Effect() successfulAuthenticated$ = this.actions$.pipe(
    ofType<authActions.Authenticated>(authActions.AuthActionTypes.Authenticated),
    map(toPayload),
    switchMap(x => {
      return this.afs.doc(`users/${x.uid}`).valueChanges();
    }),
    filter(x => !!x),
    flatMap((x: User) => {
      return [new authActions.SaveUserData(x), new authActions.CheckEmailVerified({
        uid: x.uid,
        emailVerified: x.emailVerified
      })];
    }),
    catchError(err => of({type: 'UNHANDLED_ERROR', payload: err}))
  );

  @Effect({dispatch: false}) checkEmailVerified$ = this.actions$.pipe(
    ofType<authActions.CheckEmailVerified>(authActions.AuthActionTypes.CheckEmailVerified),
    map(toPayload),
    switchMap(x => this.afAuth.authState.pipe(
      tap(authData => {
        if (authData.emailVerified !== x.emailVerified) {
          this.afs.doc(`users/${x.uid}`).update({
            emailVerified: true
          });
        }
      }),
      catchError(err => of({type: 'UNHANDLED_ERROR', payload: err}))
    ))
  );

  @Effect() signIn$ = this.actions$.pipe(
    ofType<authActions.SignIn>(authActions.AuthActionTypes.SignIn),
    map(toPayload),
    switchMap(x => {
      return from(this.afAuth.auth.signInWithEmailAndPassword(x.login, x.password)).pipe(
        map(x => {
          this.router.navigate(['main']);
          return new authActions.SignInCompleted();
        }),
        catchError(err => of(new authActions.AuthOperationFailed(err)))
      );
    }),
  );

  @Effect({dispatch: false}) signOut$ = this.actions$.pipe(
    ofType<authActions.SignOut>(authActions.AuthActionTypes.SignOut),
    switchMap(x => {
      return from(this.afAuth.auth.signOut()).pipe(
        catchError(err => of({type: 'UNHANDLED_ERROR', payload: err}))
      );
    })
  );


  @Effect() EmailPasswordSignUp$ = this.actions$.pipe(
    ofType<authActions.SignUp>(authActions.AuthActionTypes.SignUp),
    map(toPayload),
    switchMap(x => from(this.afAuth.auth.createUserWithEmailAndPassword(x.email, x.password)).pipe(
      flatMap(res => [new authActions.InitUserDataAfterSignUp({
        user: res.user,
        options: {
          type: 'password',
          firstName: x.firstName,
          lastName: x.lastName
        }
      }), new authActions.SendVerificationMail()]),
      catchError(err => of(new authActions.AuthOperationFailed(err)))
    ))
  );

  @Effect({dispatch: false}) saveUserDataAfterSignUp$ = this.actions$.pipe(
    ofType<authActions.InitUserDataAfterSignUp>(authActions.AuthActionTypes.InitUserDataAfterSignUp),
    map(toPayload),
    switchMap(x => this.afs.doc(`users/${x.user.uid}`).get().pipe(
      switchMap(data => {
        if (!data.exists) {
          const userData: User = {
            uid: x.user.uid,
            email: x.user.email,
            displayName: x.user.displayName ? x.user.displayName : `${x.options.firstName} ${x.options.lastName}`,
            firstName: x.options.firstName ? x.options.firstName : null,
            lastName: x.options.lastName ? x.options.lastName : null,
            emailVerified: x.user.emailVerified,
            provider: x.options.type
          };
          return from(this.afs.doc(`users/${x.user.uid}`).set(userData)).pipe(
            filter(y => x.options.type !== 'password'),
            tap(y => {
              this.router.navigate(['main']);
            }),
            catchError(err => of(new authActions.AuthOperationFailed(err)))
          );
        } else {
          return this.router.navigate(['main']);
        }
      }),
      catchError(err => of(new authActions.AuthOperationFailed(err)))
    )),
  );

  @Effect({dispatch: false}) sendVerificationMail$ = this.actions$.pipe(
    ofType<authActions.SendVerificationMail>(authActions.AuthActionTypes.SendVerificationMail),
    switchMap(x => {
      return this.afAuth.auth.currentUser.sendEmailVerification();
    }),
    map(x => {
      this.router.navigate(['auth/registration-successed'], {
        queryParams: {
          email: this.afAuth.auth.currentUser.email
        },
        skipLocationChange: true
      });
    }),
    catchError(err => of({type: 'UNHANDLED_ERROR', payload: err}))
  );

  @Effect() socialNetworkSignIn$ = this.actions$.pipe(
    ofType<authActions.SocialNetworkSignIn>(authActions.AuthActionTypes.SocialNetworkSignIn),
    map(toPayload),
    switchMap(x => {
      let provider;
      switch (x.type) {
        case 'google':
          provider = new auth.GoogleAuthProvider();
          break;
        case 'facebook':
          provider = new auth.FacebookAuthProvider();
          break;
      }
      return from(this.afAuth.auth.signInWithPopup(provider)).pipe(
        map(res => new authActions.InitUserDataAfterSignUp({
          user: res.user,
          options: {
            type: x.type
          }
        })),
        catchError(err => of(new authActions.AuthOperationFailed(err)))
      );
    })
  );
}
