import {User} from '../model/models';
import {AuthActions, AuthActionTypes} from './auth.actions';

export interface ErrorObject {
  code: string;
  message: string;
}

export interface AuthState {
  user: User;
  error: ErrorObject;
  loading: boolean;
}

export function reducer(state: AuthState = initialState, action: AuthActions): AuthState {
  switch (action.type) {
    case AuthActionTypes.GetUser: {
      return {
        ...state,
        loading: true
      };
    }
    case AuthActionTypes.NotAuthenticated: {
      return {
        ...state,
        user: null,
        loading: false
      };
    }
    case AuthActionTypes.SaveUserData: {
      return {
        ...state,
        user: action.payload,
        loading: false
      };
    }
    case AuthActionTypes.AuthOperationFailed: {
      return {
        ...state,
        error: action.payload
      };
    }
    default:
      return {
        ...state,
        error: null
      };
  }
}

const initialState: AuthState = {
  user: null,
  error: null,
  loading: false
};
