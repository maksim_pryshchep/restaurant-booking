import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ContainerComponent } from './container/container.component';
import { RegistrationSuccessedComponent } from './registration-successed/registration-successed.component';
import { AuthGuard } from './guards/auth.guard';

const childAuthRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'registration', component: RegisterComponent },
    { path: 'registration-successed', component: RegistrationSuccessedComponent}
];

const authRoutes: Routes = [
    { path: 'auth', component: ContainerComponent, children: childAuthRoutes, canActivate: [AuthGuard] },
];

@NgModule({
    imports: [
        RouterModule.forChild(authRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AuthRoutingModule {
}
