import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-registration-successed',
  templateUrl: './registration-successed.component.html',
  styleUrls: ['./registration-successed.component.scss']
})
export class RegistrationSuccessedComponent implements OnInit {
  emailValue: string;
  constructor(private activatedRoute: ActivatedRoute) {
    this.emailValue = this.activatedRoute.snapshot.queryParamMap.get('email');
  }

  ngOnInit(): void {
  }

}
