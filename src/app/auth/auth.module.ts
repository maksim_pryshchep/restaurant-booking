import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NzButtonModule, NzInputModule } from 'ng-zorro-antd';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ContainerComponent } from './container/container.component';
import { AuthRoutingModule } from './auth-routing.module';
import { faEye } from '@fortawesome/free-regular-svg-icons';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducer } from './state/auth.reducer';
import { AuthEffects } from './state/auth.effects';
import { RegistrationSuccessedComponent } from './registration-successed/registration-successed.component';
import { AuthGuard } from './guards/auth.guard';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NzButtonModule,
        FontAwesomeModule,
        AuthRoutingModule,
        NzInputModule,
        NzButtonModule,
        ReactiveFormsModule,
        StoreModule.forFeature('auth', reducer),
        EffectsModule.forFeature([AuthEffects])
    ],
    declarations: [
        RegisterComponent,
        LoginComponent,
        ContainerComponent,
        RegistrationSuccessedComponent
    ],
    providers: [
        AuthGuard
    ]
})
export class AuthModule {
    constructor(private library: FaIconLibrary) {
        library.addIcons(faEye);
    }
}
