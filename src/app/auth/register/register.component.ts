import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { State } from '../../state/app.state';
import * as fromAuth from '../state/auth.actions';
import { auth } from 'firebase';
import { Subscription } from 'rxjs';
import { lastError } from '../state/auth.selectors';
import { filter } from 'rxjs/operators';
import { ErrorObject } from '../state/auth.reducer';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registrationForm: FormGroup;
  registrationSuccessed = false;
  registerError = false;
  registerError$: Subscription;

  constructor(private fb: FormBuilder, private store: Store<State>) {
    this.registerError$ = this.store.pipe(select(lastError),
      filter((x: ErrorObject) => x && (x.code === 'auth/email-already-in-use')))
      .subscribe(res => {
          this.registerError = true;
        }
      );
  }

  ngOnInit() {
    this.registrationForm = this.fb.group({
      firstName: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(8)]],
      phoneNumber: [null, [Validators.required, Validators.pattern(new RegExp('\\+375(29|44|33|25)[0-9]{7}'))]],
      confirmPassword: [null, [Validators.required]]
    }, { validator: this.checkPasswords });
  }

  checkPasswords(group: FormGroup) {
    const pass = group.get('password').value;
    const confirmPass = group.get('confirmPassword').value;

    return pass === confirmPass ? null : { notSame: true };
  }

  signUp() {
    if (!this.registrationForm.valid) {
      this.validateForm();
    } else {
      this.store.dispatch(new fromAuth.SignUp({
        email: this.registrationForm.get('email').value.trim(),
        password: this.registrationForm.get('password').value,
        firstName: this.registrationForm.get('firstName').value.trim(),
        lastName:  this.registrationForm.get('lastName').value.trim(),
        phoneNumber: this.registrationForm.get('phoneNumber').value.trim()
      }));
    }
  }

  validateForm() {
    for (const i in this.registrationForm.controls) {
      this.registrationForm.controls[i].markAsDirty();
      this.registrationForm.controls[i].updateValueAndValidity();
      this.registrationForm.controls[i].markAsTouched();
    }
  }

}
