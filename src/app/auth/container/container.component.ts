import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../state/app.state';
import * as fromAuth from '../state/auth.actions';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {

  constructor(private store: Store<State>) { }

  ngOnInit() {
  }

  signInWithGoogle() {
    this.store.dispatch(new fromAuth.SocialNetworkSignIn({
      type: 'google'
    }));
  }

  signInWithFacebook() {
    this.store.dispatch(new fromAuth.SocialNetworkSignIn({
      type: 'facebook'
    }));
  }

}
