import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {Observable} from "rxjs";
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
 
@Injectable()
export class AuthGuard implements CanActivate{

    constructor(private router: Router, private afAuth: AngularFireAuth) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<boolean> | boolean {
        return true;
    }
}