import { Restaurant } from '../../restaurants/model/models';

export interface FirebaseUser {
    uid: string;
    email: string;
    displayName: string;
    photoURL: string;
    emailVerified: boolean;
}

export interface User {
    uid: string;
    email: string;
    firstName: string;
    lastName: string;
    emailVerified: boolean;
    displayName: string;
    provider: string;
}

export interface SignUpOptions {
    type: string;
    firstName?: string;
    lastName?: string;
}
