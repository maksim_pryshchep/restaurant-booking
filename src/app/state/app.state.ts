import * as fromRouter from '@ngrx/router-store';
import { RestaurantsState } from '../restaurants/state/restaurants.reducer';

export interface State {
  router: fromRouter.RouterReducerState<any>;
  restaurants: RestaurantsState;
}
