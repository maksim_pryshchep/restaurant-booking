import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import {
  NZ_I18N,
  NzButtonModule,
  NzDatePickerModule,
  NzInputNumberModule,
  NzModalModule,
  NzPopoverModule,
  NzSelectModule,
  NzTimePickerModule,
  ru_RU
} from 'ng-zorro-antd';
import { MainPageComponent } from './main-page/main-page.component';
import { registerLocaleData } from '@angular/common';
import ru from '@angular/common/locales/ru';
import { HttpClientModule } from '@angular/common/http';
import { RestaurantsModule } from './restaurants/restaurants.module';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCalendarDay, faList, faSearch } from '@fortawesome/free-solid-svg-icons';
import { faNewspaper } from '@fortawesome/free-regular-svg-icons';
import { AuthModule } from './auth/auth.module';
import { routerReducer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AgmCoreModule } from '@agm/core';
import { EffectsModule } from '@ngrx/effects';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NotFoundPageComponent } from './not-found/not-found-page.component';
import { BookingModule } from './booking/booking.module';
import localeRu from '@angular/common/locales/ru';
import { ProfileModule } from './profile/profile.module';

registerLocaleData(localeRu, 'ru');


@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    NotFoundPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NzButtonModule,
    NzDatePickerModule,
    NzTimePickerModule,
    NzInputNumberModule,
    NzSelectModule,
    HttpClientModule,
    AppRoutingModule,
    RestaurantsModule,
    NzSpinModule,
    BookingModule,
    StoreModule.forRoot({
      router: routerReducer,
    }),
    EffectsModule.forRoot([]),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    NzModalModule,
    StoreRouterConnectingModule.forRoot(),
    FontAwesomeModule,
    AuthModule,
    ProfileModule,
    StoreDevtoolsModule.instrument({
      name: 'App Devtools',
      maxAge: 25,
      logOnly: environment.production
    }),
    AgmCoreModule.forRoot(),
    NzPopoverModule
  ],
  providers: [{ provide: NZ_I18N, useValue: ru_RU }, { provide: LOCALE_ID, useValue: 'ru' }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faList, faNewspaper, faSearch, faCalendarDay);
  }
}
