import { Component, Input } from '@angular/core';
import { ReservationRecord } from '../../../booking/model/models';
import { firestore } from 'firebase';

@Component({
  selector: 'app-reservation-record',
  templateUrl: './reservation-record.component.html',
  styleUrls: ['./reservation-record.component.scss']
})
export class ReservationRecordComponent {
  @Input() reservation: ReservationRecord;



  getDate(date: firestore.Timestamp) {
    return date.toDate();
  }
}
