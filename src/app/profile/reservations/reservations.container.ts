import { Component } from '@angular/core';
import { State } from '../../state/app.state';
import { select, Store } from '@ngrx/store';
import * as profileActions from '../state/profile.actions';
import { getReservations } from '../state/profile.selector';

@Component({
  selector: 'app-reservations-container',
  templateUrl: './reservations.container.html',
  styleUrls: ['./reservations.container.scss']
})
export class ReservationsContainer {

  reservations$;

  constructor(private store: Store<State>) {
    this.store.dispatch(new profileActions.LoadReservations());
    this.reservations$ = this.store.pipe(select(getReservations));
  }
}
