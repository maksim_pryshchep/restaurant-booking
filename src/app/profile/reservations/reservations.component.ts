import { Component, Input } from '@angular/core';
import { ReservationRecord } from '../../booking/model/models';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.scss']
})
export class ReservationsComponent {
  @Input() reservations: ReservationRecord[] = [];

  getReservations(isFuture) {
    const currentDate = new Date();
    if (!this.reservations) {
      return [];
    }
    return  isFuture ? this.reservations.filter(elem => elem.timeFrom.toDate().valueOf() >= currentDate.valueOf()) :
      this.reservations.filter(elem => elem.timeFrom.toDate().valueOf() < currentDate.valueOf());
  }

}
