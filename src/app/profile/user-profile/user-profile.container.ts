import { Component } from '@angular/core';
import { State } from '../../state/app.state';
import { select, Store } from '@ngrx/store';
import { getCurrentUser } from '../../auth/state/auth.selectors';

@Component({
  selector: 'app-user-profile-container',
  templateUrl: './user-profile.container.html',
  styleUrls: ['./user-profile.container.scss']
})
export class UserProfileContainer {

  currentUser$;

  constructor(private store: Store<State>) {
    this.currentUser$ = this.store.pipe(select(getCurrentUser));
  }
}
