import { Component, Input, OnInit } from '@angular/core';
import { User } from 'firebase';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  @Input() currentUser: User;
  menuSelectedItem: string;

  constructor(private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.menuSelectedItem = this.activatedRoute.snapshot.firstChild.routeConfig.path ;
  }

  getIconString() {
    let res = '';
    this.currentUser.displayName.split(' ').forEach(elem => {
      res += elem.charAt(0);
    });
    return res;

  }

}
