import { ProfileActions, ProfileActionTypes } from './profile.actions';
import { ReservationRecord } from '../../booking/model/models';
import { Restaurant } from '../../restaurants/model/models';

export interface ProfileState {
  reservations: ReservationRecord[];
  favorites: Restaurant[];
}

export function reducer(state: ProfileState = initialState, action: ProfileActions): ProfileState {
  switch (action.type) {
    case ProfileActionTypes.StoreReservations:
      return {
        ...state,
        reservations: action.payload
      };
    case ProfileActionTypes.StoreFavorites:
      return  {
        ...state,
        favorites: action.payload
      }
    default:
      return {
        ...state
      };
  }
}

const initialState: ProfileState = {
  reservations: null,
  favorites: null
};
