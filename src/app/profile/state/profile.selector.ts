import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ProfileState } from './profile.reducer';

const getProfileFeatureState = createFeatureSelector<ProfileState>('profile');

export const getReservations = createSelector(
  getProfileFeatureState,
  state => state.reservations
);

export const getFavorites = createSelector(
  getProfileFeatureState,
  state => state.favorites
);
