import { ReservationRecord } from '../../booking/model/models';
import { Action } from '@ngrx/store';
import { Restaurant } from '../../restaurants/model/models';

export enum ProfileActionTypes {
  LoadReservations = '[Profile] Load Reservations',
  StoreReservations = '[Profile] Store Reservations',
  LoadFavorites = '[Profile] Load Favorites',
  StoreFavorites = '[Profile] Store Favorites',
  DeleteFromFavorites = '[Profile] Delete From Favorites'
}

export class DeleteFromFavorites {
  readonly type = ProfileActionTypes.DeleteFromFavorites;

  constructor(public payload: Restaurant) {
  }

}

export class LoadReservations implements Action {
  readonly type = ProfileActionTypes.LoadReservations;
}

export class StoreReservations implements Action {
  readonly type = ProfileActionTypes.StoreReservations;
  constructor(public payload: ReservationRecord[]) {

  }
}

export class LoadFavorites implements Action {
  readonly  type = ProfileActionTypes.LoadFavorites;
}

export class StoreFavorites implements Action {
  readonly type = ProfileActionTypes.StoreFavorites;
  constructor(public payload: Restaurant[]) {
  }
}

export type ProfileActions = DeleteFromFavorites | StoreFavorites | LoadReservations | StoreReservations | LoadFavorites;
