import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { State } from '../../state/app.state';
import * as profileActions from './profile.actions';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, switchMap, withLatestFrom } from 'rxjs/operators';
import { getCurrentUser } from '../../auth/state/auth.selectors';
import { ReservationRecord } from '../../booking/model/models';
import { Restaurant } from '../../restaurants/model/models';
import { toPayload } from '../../auth/state/auth.effects';

@Injectable()
export class ProfileEffects {
  constructor(private actions$: Actions,
              private store$: Store<State>,
              private afs: AngularFirestore) {

  }


  @Effect() loadReservations$ = this.actions$.pipe(
    ofType<profileActions.LoadReservations>(profileActions.ProfileActionTypes.LoadReservations),
    withLatestFrom(
      this.store$.select(getCurrentUser),
      (payload, user) => ({
        payload,
        userId: user.uid
      })
    ),
    switchMap(x => {
      return this.afs.collection('users').doc(x.userId).collection<ReservationRecord>('orders').valueChanges().pipe(
        map(y => {
          return new profileActions.StoreReservations(y);
        })
      );
    })
  );

  @Effect() loadFavorites$ = this.actions$.pipe(
    ofType<profileActions.LoadFavorites>(profileActions.ProfileActionTypes.LoadFavorites),
    withLatestFrom(
      this.store$.select(getCurrentUser),
      (payload, user) => ({
        payload,
        userId: user.uid
      })
    ),
    switchMap(x => {
      return this.afs.collection<Restaurant>('restaurants',
        ref => ref.where('inFavorites', 'array-contains', x.userId)).valueChanges().pipe(
        map(y => {
          return new profileActions.StoreFavorites(y);
        })
      );
    })
  );

  @Effect({ dispatch: false }) deleteToFavorites$ = this.actions$.pipe(
    ofType<profileActions.DeleteFromFavorites>(profileActions.ProfileActionTypes.DeleteFromFavorites),
    map(toPayload),
    withLatestFrom(
      this.store$.select(getCurrentUser),
      (payload, user) => ({
        restaurant: payload,   user
      })
    ),
    switchMap(x => {
      x.restaurant.inFavorites = x.restaurant.inFavorites.filter(elem => elem !== x.user.uid);
      return this.afs.collection('restaurants').doc(x.restaurant.uid).update({
        inFavorites: x.restaurant.inFavorites
      });
    })
  );
}
