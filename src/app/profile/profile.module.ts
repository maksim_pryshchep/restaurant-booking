import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ProfileRouterModule } from './profile-router.module';
import { UserProfileContainer } from './user-profile/user-profile.container';
import { reducer } from './state/profile.reducer';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ProfileEffects } from './state/profile.effects';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { NgZorroAntdModule, NzAvatarModule, NzButtonModule, NzMenuModule, NzTagModule, NzToolTipModule } from 'ng-zorro-antd';
import { ReservationsComponent } from './reservations/reservations.component';
import { ReservationsContainer } from './reservations/reservations.container';
import { FavoritesComponent } from './favorites/favorites.component';
import { FavoritesContainer } from './favorites/favorites.container';
import { ReservationRecordComponent } from './reservations/reservation-record/reservation-record.component';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { RestaurantsModule } from '../restaurants/restaurants.module';

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        StoreModule.forFeature('profile', reducer),
        EffectsModule.forFeature([ProfileEffects]),
        ProfileRouterModule,
        NzAvatarModule,
        NzTagModule,
        NzToolTipModule,
        NzMenuModule,
        NzButtonModule,
        RestaurantsModule,
        NgZorroAntdModule
    ],
  declarations: [
    UserProfileContainer,
    UserProfileComponent,
    ReservationsComponent,
    ReservationsContainer,
    FavoritesContainer,
    FavoritesComponent,
    ReservationRecordComponent
  ],
  entryComponents: []
})
export class ProfileModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faCheckCircle);
  }
}
