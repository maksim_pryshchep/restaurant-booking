import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserProfileContainer } from './user-profile/user-profile.container';
import { ReservationsContainer } from './reservations/reservations.container';
import { FavoritesContainer } from './favorites/favorites.container';

const profileRoutesChildRoutes: Routes = [
  { path: '', redirectTo: 'reservations', pathMatch: 'full' },
  { path: 'reservations', component: ReservationsContainer },
  { path: 'favorites', component: FavoritesContainer },
];


const profileRoutes: Routes = [
  { path: 'profile', component: UserProfileContainer, children: profileRoutesChildRoutes  }
];

@NgModule({
  imports: [
    RouterModule.forChild(profileRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProfileRouterModule {
}
