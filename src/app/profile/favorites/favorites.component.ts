import { Component, Input } from '@angular/core';
import { Restaurant } from '../../restaurants/model/models';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent {

  @Input() favorites: Restaurant[];

  constructor() {
  }
}
