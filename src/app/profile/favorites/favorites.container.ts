import { Component } from '@angular/core';
import { State } from '../../state/app.state';
import { select, Store } from '@ngrx/store';
import * as profileActions from '../state/profile.actions';
import { getFavorites } from '../state/profile.selector';

@Component({
  selector: 'app-favorites-container',
  templateUrl: './favorites.container.html',
  styleUrls: ['./favorites.container.scss']
})
export class FavoritesContainer {

  favorites$;

  constructor(private store: Store<State>) {
    this.store.dispatch(new profileActions.LoadFavorites());
    this.favorites$ = this.store.pipe(select(getFavorites));
  }
}
