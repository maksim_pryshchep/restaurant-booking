import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { RestaurantsListComponent } from './restaurants-list/restaurants-list.component';
import { RestaurantProfileComponent } from './restaurant-profile/restaurant-profile.component';
import { RestaurantsRoutingModule } from './restaurants-routing.module';
import {
  NgZorroAntdModule, NZ_ICONS,
  NzAffixModule,
  NzButtonModule,
  NzCheckboxModule, NzCollapseModule, NzCommentModule,
  NzDatePickerModule, NzDividerModule, NzInputModule,
  NzPopoverModule,
  NzProgressModule,
  NzRadioModule,
  NzSelectModule,
  NzTimePickerModule
} from 'ng-zorro-antd';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faBookmark,
  faChartBar,
  faClock,
  faCommentDots,
  faCreditCard,
  faEye,
  faHeart,
  faImages,
  faListAlt,
  faMap,
  faMoneyBillAlt,
  faNewspaper,
  faShareSquare,
  faStar,
  faThumbsUp
} from '@fortawesome/free-regular-svg-icons';
import {
  faBookOpen,
  faBusinessTime,
  faChevronDown,
  faCoins,
  faDiceFive,
  faDollarSign,
  faHome,
  faInfo,
  faMapMarkedAlt,
  faMapMarkerAlt,
  faMusic,
  faParking,
  faPhoneAlt,
  faShare,
  faStar as fasStar,
  faTags,
  faUtensils,
  faPlus,
  faMinus,
  faHeart as fasHeart,
  faTrashAlt
} from '@fortawesome/free-solid-svg-icons';
import { AgmCoreModule } from '@agm/core';
import { CheckboxGroupComponent } from './restaurants-list/checkbox-group/checkbox-group.component';
import { ListItemComponent } from './restaurants-list/list-item/list-item.component';
import { MapComponent } from './restaurants-list/map/map.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from './state/restaurants.reducer';
import { EffectsModule } from '@ngrx/effects';
import { RestaurantsEffects } from './state/restaurants.effects';
import { UtilsService } from '../services/utils.service';
import { RestaurantProfileContainer } from './restaurant-profile/restaurant-profile.container';
import { GalleryComponent } from './restaurant-profile/gallery/gallery.component';
import { MainComponent } from './restaurant-profile/main/main.component';
import { MenuComponent } from './restaurant-profile/menu/menu.component';
import { NewsComponent } from './restaurant-profile/news/news.component';
import { ReviewsComponent } from './restaurant-profile/reviews/reviews.component';
import { MainContainer } from './restaurant-profile/main/main.container';
import { ReviewsContainer } from './restaurant-profile/reviews/reviews.container';
import { NewsContainer } from './restaurant-profile/news/news.container';
import { MenuContainer } from './restaurant-profile/menu/menu.container';
import { GalleryContainer } from './restaurant-profile/gallery/gallery.container';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { MenuItemComponent } from './restaurant-profile/menu/menu-item/menu-item.component';
import { ReviewItemComponent } from './restaurant-profile/reviews/review-item/review-item.component';
import { IconDefinition } from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons = Object.keys(antDesignIcons).map(key => antDesignIcons[key]);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RestaurantsRoutingModule,
    NzButtonModule,
    FontAwesomeModule,
    NzProgressModule,
    AgmCoreModule,
    NzDatePickerModule,
    NzTimePickerModule,
    NzSelectModule,
    NzCheckboxModule,
    NzPopoverModule,
    NgxGalleryModule,
    NzRadioModule,
    NzAffixModule,
    NzDividerModule,
    NzInputModule,
    NzCollapseModule,
    NzCommentModule,
    StoreModule.forFeature('restaurants', reducer),
    EffectsModule.forFeature([RestaurantsEffects]),
    NgZorroAntdModule
  ],
  declarations: [
    RestaurantProfileComponent,
    RestaurantsListComponent,
    CheckboxGroupComponent,
    ListItemComponent,
    MapComponent,
    RestaurantProfileContainer,
    MainComponent,
    MainContainer,
    MenuComponent,
    MenuContainer,
    NewsComponent,
    NewsContainer,
    ReviewsComponent,
    ReviewsContainer,
    GalleryComponent,
    GalleryContainer,
    MenuItemComponent,
    ReviewItemComponent
  ],
  exports: [
    ListItemComponent
  ],
  providers: [UtilsService, { provide: NZ_ICONS, useValue: icons }]
})

export class RestaurantsModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(
      faBookmark,
      faShare,
      faHome,
      faListAlt,
      faImages,
      faBookOpen,
      faNewspaper,
      faCommentDots,
      faTags,
      faCreditCard,
      faParking,
      faMusic,
      faBusinessTime,
      faStar,
      fasStar,
      faThumbsUp,
      faClock,
      faCoins,
      faDollarSign,
      faInfo,
      faPhoneAlt,
      faMapMarkerAlt,
      faChartBar,
      faEye,
      faHeart,
      faShareSquare,
      faMapMarkedAlt,
      faMap,
      faMoneyBillAlt,
      faUtensils,
      faDiceFive,
      faChevronDown,
      faPlus,
      faMinus,
      fasHeart,
      faTrashAlt
    );
  }
}
