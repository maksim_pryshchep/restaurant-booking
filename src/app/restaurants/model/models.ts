import { firestore } from 'firebase';

export interface ByKeyList<T> {
  [key: string]: T;
}

export interface BaseRating {
  atmosphere: number;
  cost: number;
  food: number;
  service: number;
}

export interface MenuItem {
  imageUrl: string;
  title: string;
  description: string;
  price: number;
  uid: string;
  count?: number;
}

export interface LocationMarker {
  lat: number;
  lng: number;
  imageUrl: string;
}

export interface TableUnit {
  uid: string;
  placesCount: number;
  tableNumber: number;
  available?: boolean;
}

export interface Restaurant {
  contactInfo: {
    address: string;
    city: string;
    telephoneNumber: string;
  };
  dateOfCreation: firestore.Timestamp;
  name: string;
  priceCategory: number;
  ratingAggregateValue: BaseRating;
  titleImageUrl: string;
  type: string;
  uid: string;
  location: firestore.GeoPoint;
  description: string;
  logoImageUrl: string;
  galleryUrls: string[];
  tables: TableUnit[];
  schemaUrl: string;
  inFavorites: string[];
}

export interface Review {
  uid: string;
  date: firestore.Timestamp;
  likes: string[];
  dislikes: string[];
  content: string;
  userId: string;
  displayName: string;
}


