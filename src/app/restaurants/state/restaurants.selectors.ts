import { createFeatureSelector, createSelector } from '@ngrx/store';
import { RestaurantsState } from './restaurants.reducer';
import { getCurrentUser } from '../../auth/state/auth.selectors';

const getRestaurantsFeatureState = createFeatureSelector<RestaurantsState>('restaurants');

export const isLoading = createSelector(
  getRestaurantsFeatureState,
  state => state.loading
);

export const getRestaurants = createSelector(
  getRestaurantsFeatureState,
  state => state.restaurants
);

export const getSelectedRestaurantId = createSelector(
  getRestaurantsFeatureState,
  state => state.selectedRestaurantId
);

export const isLoadingRestaurants = createSelector(
  getRestaurantsFeatureState,
  state => state.loading
);

export const getCurrentRestaurantProfile = createSelector(
  getRestaurantsFeatureState,
  state => state.currentRestaurant
);

export const getRestaurantMenu = createSelector(
  getRestaurantsFeatureState,
  state => state.menu
);

export const getRestaurantsTypeFilerOptions = createSelector(
  getRestaurantsFeatureState,
  state => state.restaurantsTypesFilterOptions
);

export const getPriceCategoryTypesFilterOptions = createSelector(
  getRestaurantsFeatureState,
  state => state.priceCategoryTypesFilterOptions
);

export const getCurrentMapBounds = createSelector(
  getRestaurantsFeatureState,
  state => state.currentMapBounds
);

export const getReviews = createSelector(
  getRestaurantsFeatureState,
  state => state.reviews
);

export const getReviewById = createSelector(
  getRestaurantsFeatureState,
  (state, props) => {
    return state.reviews ? state.reviews.find(elem => elem.uid === props.id) : null;
  }
);

export const getFilteredItems = createSelector(
  getRestaurants,
  getRestaurantsTypeFilerOptions,
  getPriceCategoryTypesFilterOptions,
  getCurrentMapBounds,
  (restaurants, restaurantsTypesFilter, priceCategoryTypesFilter, mapBounds) => {
    if (!restaurantsTypesFilter) {
      return [];
    }
    const restaurantsTypesFilterIsClear = !restaurantsTypesFilter.find(elem => elem.checked);
    const priceCategoryTypesFilterIsClear = !priceCategoryTypesFilter.find(elem => elem.checked);

    if (!restaurantsTypesFilterIsClear) {
      restaurants = restaurants.filter(elem => restaurantsTypesFilter.find(filter => filter.value === elem.type && filter.checked));
    }

    if (!priceCategoryTypesFilterIsClear) {
      restaurants = restaurants.filter(elem => priceCategoryTypesFilter.find(filter => filter.value === elem.priceCategory &&
        filter.checked));
    }

    if (mapBounds) {
      restaurants = restaurants.filter(elem => elem.location.latitude >= mapBounds[0].lat() && elem.location.latitude <= mapBounds[1].lat()
        && elem.location.longitude >= mapBounds[0].lng() && elem.location.longitude <= mapBounds[1].lng());
    }

    return restaurants;
  }
);
