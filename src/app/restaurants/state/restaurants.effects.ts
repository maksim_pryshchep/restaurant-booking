import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as restaurantsActions from './restaurants.actions';
import { AngularFirestore } from '@angular/fire/firestore';
import { flatMap, map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { MenuItem, Restaurant, Review } from '../model/models';
import { Store } from '@ngrx/store';
import { State } from '../../state/app.state';
import { toPayload } from '../../auth/state/auth.effects';
import { Router } from '@angular/router';
import { getCurrentRestaurantProfile, getReviewById } from './restaurants.selectors';
import { getCurrentUser } from '../../auth/state/auth.selectors';
import { firestore } from 'firebase';
import { from } from 'rxjs';


@Injectable()
export class RestaurantsEffects {
  constructor(private actions$: Actions, private afs: AngularFirestore, private store$: Store<State>, private router: Router) {

  }

  @Effect() loadRestaurantProfile$ = this.actions$.pipe(
    ofType<restaurantsActions.LoadRestaurantProfile>(restaurantsActions.RestaurantsActionTypes.LoadRestaurantProfile),
    map(toPayload),
    switchMap(data => {
      return this.afs.doc<Restaurant>(`restaurants/${data.uid}`).valueChanges().pipe(
        tap(x => {
          if (!x) {
            this.router.navigateByUrl('not-found');
          }
        }),
        map(x => x ? new restaurantsActions.LoadRestaurantProfileComplete(x) : new restaurantsActions.LoadRestaurantProfileComplete(null))
      );
    })
  );

  @Effect() loadRestaurants$ = this.actions$.pipe(
    ofType<restaurantsActions.LoadRestaurants>(restaurantsActions.RestaurantsActionTypes.LoadRestaurants),
    switchMap(x => {
      const restaurantCollection = this.afs.collection<Restaurant>('restaurants');
      return restaurantCollection.valueChanges();
    }),
    flatMap(x => {
      const restaurantsTypes = {};
      x.forEach(elem => {
        restaurantsTypes[elem.type] = {
          value: elem.type,
          label: elem.type,
          checked: false
        };
      });
      return [new restaurantsActions.SetRestaurants(x),
        new restaurantsActions.SetRestaurantsTypesFilterOptions(Object.values(restaurantsTypes))];
    })
  );

  @Effect() loadReviews$ = this.actions$.pipe(
    ofType<restaurantsActions.LoadReview>(restaurantsActions.RestaurantsActionTypes.LoadReviews),
    withLatestFrom(
      this.store$.select(getCurrentRestaurantProfile),
      (payload, restaurant) => ({ restaurant })
    ),
    switchMap(x => {
      return this.afs.collection('restaurants').doc(x.restaurant.uid).collection('reviews').valueChanges().pipe(
        map((y: Review[]) => {
          return new restaurantsActions.StoreReviews(y);
        })
      );
    })
  );

  @Effect({ dispatch: false }) appendReview$ = this.actions$.pipe(
    ofType<restaurantsActions.AppendReview>(restaurantsActions.RestaurantsActionTypes.AppendReview),
    map(toPayload),
    withLatestFrom(
      this.store$.select(getCurrentUser),
      this.store$.select(getCurrentRestaurantProfile),
      (payload, user, restaurant) => ({
        payload,
        user,
        restaurant
      })
    ),
    switchMap(x => {
      return from(this.afs.collection('restaurants').doc(x.restaurant.uid).collection('reviews').add({
        date: firestore.Timestamp.fromDate(new Date()),
        userId: x.user.uid,
        dislikes: [],
        likes: [],
        content: x.payload,
        displayName: x.user.displayName
      })).pipe(
        switchMap(y => {
          return y.update({
            uid: y.id
          });
        })
      );
    })
  );

  @Effect({ dispatch: false }) addLike$ = this.actions$.pipe(
    ofType<restaurantsActions.AddLike>(restaurantsActions.RestaurantsActionTypes.AddLike),
    map(toPayload),
    withLatestFrom(
      this.store$.select(getCurrentUser),
      this.store$.select(getCurrentRestaurantProfile),
      (payload, user, restaurant) => ({
        reviewId: payload,
        user,
        restaurant
      })
    ),
    switchMap(x => {
      return this.store$.select(getReviewById, { id: x.reviewId }).pipe(
        take(1),
        switchMap(y => {
          y.likes.push(x.user.uid);
          return this.afs.collection('restaurants').doc(x.restaurant.uid).collection('reviews').doc(x.reviewId).update({
            likes: y.likes
          });
        })
      );
    })
  );

  @Effect({ dispatch: false }) removeLike$ = this.actions$.pipe(
    ofType<restaurantsActions.RemoveLike>(restaurantsActions.RestaurantsActionTypes.RemoveLike),
    map(toPayload),
    withLatestFrom(
      this.store$.select(getCurrentUser),
      this.store$.select(getCurrentRestaurantProfile),
      (payload, user, restaurant) => ({
        reviewId: payload,
        user,
        restaurant
      })
    ),
    switchMap(x => {
      return this.store$.select(getReviewById, { id: x.reviewId }).pipe(
        take(1),
        switchMap(y => {
          const likes = y.likes.filter(elem => elem !== x.user.uid);
          return this.afs.collection('restaurants').doc(x.restaurant.uid).collection('reviews').doc(x.reviewId).update({
            likes
          });
        })
      );
    })
  );

  @Effect({ dispatch: false }) addDislike$ = this.actions$.pipe(
    ofType<restaurantsActions.AddDislike>(restaurantsActions.RestaurantsActionTypes.AddDislike),
    map(toPayload),
    withLatestFrom(
      this.store$.select(getCurrentUser),
      this.store$.select(getCurrentRestaurantProfile),
      (payload, user, restaurant) => ({
        reviewId: payload,
        user,
        restaurant
      })
    ),
    switchMap(x => {
      return this.store$.select(getReviewById, { id: x.reviewId }).pipe(
        take(1),
        switchMap(y => {
          y.dislikes.push(x.user.uid);
          return this.afs.collection('restaurants').doc(x.restaurant.uid).collection('reviews').doc(x.reviewId).update({
            dislikes: y.dislikes
          });
        })
      );
    })
  );

  @Effect({ dispatch: false }) removeDislike$ = this.actions$.pipe(
    ofType<restaurantsActions.RemoveDislike>(restaurantsActions.RestaurantsActionTypes.RemoveDislike),
    map(toPayload),
    withLatestFrom(
      this.store$.select(getCurrentUser),
      this.store$.select(getCurrentRestaurantProfile),
      (payload, user, restaurant) => ({
        reviewId: payload,
        user,
        restaurant
      })
    ),
    switchMap(x => {
      return this.store$.select(getReviewById, { id: x.reviewId }).pipe(
        take(1),
        switchMap(y => {
          const dislikes = y.dislikes.filter(elem => elem !== x.user.uid);
          return this.afs.collection('restaurants').doc(x.restaurant.uid).collection('reviews').doc(x.reviewId).update({
            dislikes
          });
        })
      );
    })
  );


  @Effect() loadRestaurantMenu$ = this.actions$.pipe(
    ofType<restaurantsActions.LoadRestaurantMenu>(restaurantsActions.RestaurantsActionTypes.LoadRestaurantMenu),
    withLatestFrom(
      this.store$.select(getCurrentRestaurantProfile),
      (payload, restaurantProfile) => ({
        payload,
        restaurantId: restaurantProfile.uid
      })
    ),
    switchMap(x => {
        return this.afs.collection<{ type: string; items: MenuItem[] }>('restaurants').doc(x.restaurantId).collection('menu')
          .valueChanges();
      }
    ),
    map(x => {
      const menu = {};
      x.forEach(elem => {
        menu[elem.type] = elem.items;
      });
      return new restaurantsActions.StoreRestaurantMenu(menu);
    })
  );

  @Effect({ dispatch: false }) addToFavorites$ = this.actions$.pipe(
    ofType<restaurantsActions.AddToFavorites>(restaurantsActions.RestaurantsActionTypes.AddToFavorites),
    withLatestFrom(
      this.store$.select(getCurrentRestaurantProfile),
      this.store$.select(getCurrentUser),
      (payload, restaurant, user) => ({
        payload, restaurant, user
      })
    ),
    switchMap(x => {
      x.restaurant.inFavorites.push(x.user.uid);
      return this.afs.collection('restaurants').doc(x.restaurant.uid).update({
        inFavorites: x.restaurant.inFavorites
      });
    })
  );
}
