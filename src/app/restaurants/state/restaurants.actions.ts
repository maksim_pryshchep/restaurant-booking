import { Action } from '@ngrx/store';
import { ByKeyList, MenuItem, Restaurant, Review } from '../model/models';
import { FilterCheckboxComponent } from '../restaurants-list/checkbox-group/checkbox-group.component';
import { LatLng } from '@agm/core';

export enum RestaurantsActionTypes {
  LoadRestaurants = '[Restaurants] Load Restaurants',
  SetRestaurants = '[Restaurants] Set Restaurants',
  OnHoverListItem = '[Restaurants] On Hover List Item',
  ClearSelectedListItemId = '[Restaurants] Clear Selected List Item Id',
  LoadRestaurantProfile = '[Restaurants] Load Restaurant Profile',
  LoadRestaurantProfileComplete = '[Restaurants] Load Restaurants Profile Complete',
  ClearCurrentRestaurantProfile = '[Restaurants] Clear Current Restaurant Profile',
  LoadRestaurantMenu = '[Restaurants] Load Restaurant Menu',
  StoreRestaurantMenu = '[Restaurants] Store Restaurant Menu',
  AddToFavorites = '[Restaurants] Add To Favorites',
  SetRestaurantsTypesFilterOptions = '[Restaurants] Set Restaurants Types Filter Options',
  SetPriceCategoryTypesFilterOptions = '[Restaurants] Set Price Category Types Filter Options',
  SetMapBounds = '[Restaurants] Set Map Bounds',
  AppendReview = '[Restaurants] Append Review',
  LoadReviews = '[Restaurants] Load Reviews',
  StoreReviews = '[Restaurants] Store Reviews',
  AddLike = '[Restaurants] Add Like',
  RemoveLike = '[Restaurants] Remove Like',
  AddDislike = '[Restaurants] Add Dislike',
  RemoveDislike = '[Restaurants] Remove Dislike'
}

export class AddLike implements Action {
  readonly type = RestaurantsActionTypes.AddLike;

  constructor(public payload: string) {
  }
}

export class RemoveLike implements Action {
  readonly type = RestaurantsActionTypes.RemoveLike;

  constructor(public payload: string) {
  }
}

export class AddDislike implements Action {
  readonly type = RestaurantsActionTypes.AddDislike;

  constructor(public payload: string) {
  }
}

export class RemoveDislike implements Action {
  readonly type = RestaurantsActionTypes.RemoveDislike;

  constructor(public payload: string) {
  }
}

export class StoreReviews implements Action {
  readonly type = RestaurantsActionTypes.StoreReviews;

  constructor(public payload: Review[]) {
  }
}

export class LoadReview implements  Action {
  readonly type = RestaurantsActionTypes.LoadReviews;
}

export class AppendReview implements Action {
  readonly type = RestaurantsActionTypes.AppendReview;
  constructor(public payload: string) {
  }
}

export class SetMapBounds implements Action {
  readonly type = RestaurantsActionTypes.SetMapBounds;
  constructor(public payload: LatLng[]) {
  }
}

export class SetRestaurantsTypesFilterOptions implements Action {
  readonly type = RestaurantsActionTypes.SetRestaurantsTypesFilterOptions;

  constructor(public  payload: FilterCheckboxComponent[]) {
  }
}

export class SetPriceCategoryTypesFilterOptions implements Action {
  readonly type = RestaurantsActionTypes.SetPriceCategoryTypesFilterOptions;

  constructor(public  payload: FilterCheckboxComponent[]) {
  }
}

export class AddToFavorites implements Action {
  readonly type = RestaurantsActionTypes.AddToFavorites;
}

export class StoreRestaurantMenu implements Action {
  readonly type = RestaurantsActionTypes.StoreRestaurantMenu;

  constructor(public payload: ByKeyList<{ menuItems: MenuItem[] }>) {
  }
}

export class LoadRestaurantMenu implements Action {
  readonly type = RestaurantsActionTypes.LoadRestaurantMenu;
}

export class ClearCurrentRestaurantProfile implements Action {
  readonly type = RestaurantsActionTypes.ClearCurrentRestaurantProfile;
}

export class LoadRestaurantProfileComplete implements Action {
  readonly type = RestaurantsActionTypes.LoadRestaurantProfileComplete;

  constructor(public  payload: Restaurant) {
  }
}

export class LoadRestaurantProfile implements Action {
  readonly type = RestaurantsActionTypes.LoadRestaurantProfile;

  constructor(public payload: { uid: string }) {
  }
}

export class ClearSelectedListItemId implements Action {
  readonly type = RestaurantsActionTypes.ClearSelectedListItemId;
}

export class OnHoverListItem implements Action {
  readonly type = RestaurantsActionTypes.OnHoverListItem;

  constructor(public payload: { index: number }) {

  }
}

export class LoadRestaurants implements Action {
  readonly type = RestaurantsActionTypes.LoadRestaurants;
}

export class SetRestaurants implements Action {
  readonly type = RestaurantsActionTypes.SetRestaurants;

  constructor(public payload: Restaurant[]) {

  }
}

export type RestaurantsActions =
  LoadRestaurants
  | SetRestaurants
  | OnHoverListItem
  | ClearSelectedListItemId
  | LoadRestaurantProfile
  | LoadRestaurantProfileComplete
  | ClearCurrentRestaurantProfile
  | LoadRestaurantMenu
  | StoreRestaurantMenu
  | AddToFavorites
  | SetRestaurantsTypesFilterOptions
  | SetPriceCategoryTypesFilterOptions
  | SetMapBounds
  | AppendReview
  | LoadReview
  | StoreReviews
  | AddLike
  | RemoveLike
  | AddDislike
  | RemoveDislike;
