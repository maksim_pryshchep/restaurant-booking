import { ByKeyList, MenuItem, Restaurant, Review } from '../model/models';
import { RestaurantsActions, RestaurantsActionTypes } from './restaurants.actions';
import { FilterCheckboxComponent } from '../restaurants-list/checkbox-group/checkbox-group.component';
import { LatLng } from '@agm/core';

export interface RestaurantsState {
  restaurants: Restaurant[];
  loading: boolean;
  selectedRestaurantId: { index: number };
  currentRestaurant: Restaurant;
  menu: ByKeyList<MenuItem[]>;
  restaurantsTypesFilterOptions: FilterCheckboxComponent[];
  priceCategoryTypesFilterOptions: FilterCheckboxComponent[];
  currentMapBounds: LatLng[];
  reviews: Review[];
}

export function reducer(state: RestaurantsState = initialState, action: RestaurantsActions) {
  switch (action.type) {
    case RestaurantsActionTypes.StoreReviews: {
      return {
        ...state,
        reviews: action.payload
      };
    }
    case RestaurantsActionTypes.SetMapBounds: {
      return {
        ...state,
        currentMapBounds: action.payload
      };
    }
    case RestaurantsActionTypes.SetRestaurantsTypesFilterOptions: {
      return {
        ...state,
        restaurantsTypesFilterOptions: [...action.payload]
      };
    }
    case RestaurantsActionTypes.SetPriceCategoryTypesFilterOptions: {
      return {
        ...state,
        priceCategoryTypesFilterOptions: [...action.payload]
      };
    }
    case RestaurantsActionTypes.StoreRestaurantMenu: {
      return {
        ...state,
        menu: action.payload
      };
    }
    case RestaurantsActionTypes.LoadRestaurants: {
      return {
        ...state,
        loading: true
      };
    }
    case RestaurantsActionTypes.SetRestaurants:
      return {
        ...state,
        restaurants: action.payload,
        loading: false
      };
    case RestaurantsActionTypes.OnHoverListItem:
      return {
        ...state,
        selectedRestaurantId: action.payload
      };
    case RestaurantsActionTypes.ClearSelectedListItemId: {
      return {
        ...state,
        selectedRestaurantId: null
      };
    }
    case RestaurantsActionTypes.LoadRestaurantProfile: {
      return {
        ...state,
        loading: true
      };
    }
    case RestaurantsActionTypes.LoadRestaurantProfileComplete: {
      return {
        ...state,
        currentRestaurant: action.payload,
        loading: false
      };
    }
    case RestaurantsActionTypes.ClearCurrentRestaurantProfile: {
      return  {
        ...state,
        currentRestaurant: null
      };
    }
    default:
      return state;
  }
}

const initialState: RestaurantsState = {
  restaurants: [],
  loading: false,
  selectedRestaurantId: null,
  currentRestaurant: null,
  menu: null,
  currentMapBounds: null,
  restaurantsTypesFilterOptions: null,
  reviews: null,
  priceCategoryTypesFilterOptions: [
    {
      value: 1,
      label: '$',
      checked: false
    },
    {
      value: 2,
      label: '$$',
      checked: false
    },
    {
      value: 3,
      label: '$$$',
      checked: false
    },
    {
      value: 4,
      label: '$$$$',
      checked: false
    }
  ]
};
