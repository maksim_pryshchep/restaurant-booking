import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  ContentChildren,
  QueryList,
  AfterViewInit,
  ViewChildren,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from '../../../state/app.state';
import * as restaurantsActions from '../../state/restaurants.actions';
import { Restaurant } from '../../model/models';
import { AgmInfoWindow, LatLngBounds } from '@agm/core';
import { getSelectedRestaurantId } from '../../state/restaurants.selectors';
import { isNumber } from 'util';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnDestroy, OnChanges {

  lat = 53.9;
  lng = 27.56667;
  zoom = 10;
  @Input() restaurants: Restaurant[];
  @ViewChildren(AgmInfoWindow) infoWindow: QueryList<AgmInfoWindow>;
  previousInfoWindow: AgmInfoWindow;

  constructor(private store: Store<State>) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.restaurants) {
      this.previousInfoWindow = null;
    }
  }


  ngOnDestroy(): void {
    this.store.dispatch(new restaurantsActions.ClearSelectedListItemId());
  }

  mapReady() {
    this.store.pipe(select(getSelectedRestaurantId)).subscribe(res => {
      if (res) {
        this.infoWindow.toArray().forEach((val, index) => {
          if (index === res.index) {
            val.open();
          } else {
            val.close();
          }
        });
      }
    });
  }

  mouseOveredMarker(infowindow: AgmInfoWindow) {
    if (this.previousInfoWindow) {
      this.previousInfoWindow.close();
    }
    this.previousInfoWindow = infowindow;
    this.previousInfoWindow.open();
  }

  onBoundsChange(event: LatLngBounds) {
    this.store.dispatch(new restaurantsActions.SetMapBounds([event.getSouthWest(), event.getNorthEast()]));
  }
}
