import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as restaurantsActions from '../../state/restaurants.actions';
import { State } from '../../../state/app.state';
import { Store } from '@ngrx/store';

export interface FilterCheckboxComponent {
  label: string;
  value: string | number;
  checked?: boolean;
}

@Component({
  selector: 'app-checkbox-group',
  templateUrl: './checkbox-group.component.html',
  styleUrls: ['./checkbox-group.component.scss']
})
export class CheckboxGroupComponent implements OnInit, OnChanges {

  @Input() parameters: FilterCheckboxComponent[];
  @Input() type: string;
  options: FilterCheckboxComponent[] = [];

  constructor(private store: Store<State>) {
  }

  ngOnInit() {
    if (this.parameters) {
      this.options = this.parameters;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.parameters && changes.parameters.currentValue) {
      this.options = this.parameters;
    }
  }

  setFilter() {
    if (this.type === 'type') {
      this.store.dispatch(new restaurantsActions.SetRestaurantsTypesFilterOptions(this.options));
    } else {
      this.store.dispatch(new restaurantsActions.SetPriceCategoryTypesFilterOptions(this.options));
    }
  }
}
