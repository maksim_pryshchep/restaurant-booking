import { Component, OnInit, Input } from '@angular/core';
import { Restaurant } from '../../model/models';
import { UtilsService } from '../../../services/utils.service';
import { State } from '../../../state/app.state';
import { Store } from '@ngrx/store';
import * as profileActions from '../../../profile/state/profile.actions';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  @Input() restaurant: Restaurant;
  @Input() mapMod: boolean;
  @Input() isProfile: boolean;

  constructor(private utilsService: UtilsService, private store: Store<State>) { }

  ngOnInit() {
  }

  getStars() {
    return this.utilsService.getStars(this.restaurant.ratingAggregateValue);
  }

  getStarPath(value: string) {
    return `../../../../assets/icons/stars/${value}.svg`;
  }

  getPriceString(value: number) {
    return value ? '$$$$'.substr(0, value) : '$$$$';
  }

  deleteItem() {
    this.store.dispatch(new profileActions.DeleteFromFavorites(this.restaurant));
  }
}
