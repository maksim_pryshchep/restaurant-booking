import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { State } from '../../state/app.state';
import * as restaurantsActions from '../state/restaurants.actions';
import { Observable } from 'rxjs';
import { Restaurant } from '../model/models';
import {
  getFilteredItems,
  getPriceCategoryTypesFilterOptions,
  getRestaurants,
  getRestaurantsTypeFilerOptions
} from '../state/restaurants.selectors';
import { FilterCheckboxComponent } from './checkbox-group/checkbox-group.component';

@Component({
  selector: 'app-restaurants-list',
  templateUrl: './restaurants-list.component.html',
  styleUrls: ['./restaurants-list.component.scss']
})
export class RestaurantsListComponent implements OnInit {

  showMap = false;
  restaurants$: Observable<Restaurant[]>;
  restaurantsTypes$: Observable<FilterCheckboxComponent[]>;
  priceCategoriesTypes$: Observable<FilterCheckboxComponent[]>;

  constructor(private store: Store<State>) {
    this.store.dispatch(new restaurantsActions.LoadRestaurants());
    this.restaurants$ = this.store.pipe(select(getFilteredItems));
    this.restaurantsTypes$ = this.store.pipe(select(getRestaurantsTypeFilerOptions));
    this.priceCategoriesTypes$ = this.store.pipe(select(getPriceCategoryTypesFilterOptions));
  }

  ngOnInit() {
  }

  onToggleMap() {
    this.showMap = !this.showMap;
  }

  mouseover(id: number) {
    if (this.showMap) {
      this.store.dispatch(new restaurantsActions.OnHoverListItem({
        index: id
      }));
    }
  }

}
