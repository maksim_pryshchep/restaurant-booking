import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RestaurantsListComponent } from './restaurants-list/restaurants-list.component';
import { RestaurantProfileContainer } from './restaurant-profile/restaurant-profile.container';
import { GalleryContainer } from './restaurant-profile/gallery/gallery.container';
import { ReviewsContainer } from './restaurant-profile/reviews/reviews.container';
import { NewsContainer } from './restaurant-profile/news/news.container';
import { MenuContainer } from './restaurant-profile/menu/menu.container';
import { MainContainer } from './restaurant-profile/main/main.container';

const restaurantsProfileChildRoutes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'gallery', component: GalleryContainer },
  { path: 'reviews', component: ReviewsContainer },
  { path: 'news', component: NewsContainer },
  { path: 'menu', component: MenuContainer },
  { path: 'main', component: MainContainer }
];

const restaurantsRoutes: Routes = [
  { path: 'restaurants', component: RestaurantsListComponent },
  { path: 'restaurants/:id', component: RestaurantProfileContainer, children: restaurantsProfileChildRoutes }
];

@NgModule({
  imports: [
    RouterModule.forChild(restaurantsRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class RestaurantsRoutingModule {
}
