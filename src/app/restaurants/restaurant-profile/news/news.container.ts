import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { State } from '../../../state/app.state';
import * as restaurantsActions from '../../state/restaurants.actions';

@Component({
  selector: 'app-news-container',
  templateUrl: './news.container.html',
  styleUrls: ['./news.container.scss']
})
export class NewsContainer {

  constructor(private activatedRoute: ActivatedRoute, private store: Store<State>) {
  }

}
