import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { State } from '../../../state/app.state';
import * as restaurantsActions from '../../state/restaurants.actions';
import { getRestaurantMenu } from '../../state/restaurants.selectors';
import { getMenuFromBookingParameters } from '../../../booking/state/booking.selector';

@Component({
  selector: 'app-menu-container',
  templateUrl: './menu.container.html',
  styleUrls: ['./menu.container.scss']
})
export class MenuContainer implements OnInit {

  restaurantMenu$;
  isBooking: boolean;

  constructor(private activatedRoute: ActivatedRoute, private store: Store<State>) {
    this.isBooking = this.activatedRoute.snapshot.queryParamMap.get('isBooking') === 'true';
    if (!this.isBooking) {
      this.store.dispatch(new restaurantsActions.LoadRestaurantMenu());
    }
  }

  ngOnInit() {
    this.restaurantMenu$ = this.isBooking ? this.store.pipe(select(getMenuFromBookingParameters)) :
      this.store.pipe(select(getRestaurantMenu));
  }

}
