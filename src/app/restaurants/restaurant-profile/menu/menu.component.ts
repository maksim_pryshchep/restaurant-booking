import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ByKeyList, MenuItem } from '../../model/models';
import * as bookingActions from '../../../booking/state/booking.actions';
import { State } from '../../../state/app.state';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnChanges {

  @Input() restaurantMenu: ByKeyList<{ menuItems: MenuItem[] }>;
  @Input() isBooking: boolean;

  radioValue = 'all';
  menuOptions = [];

  constructor(private router: Router, private store: Store<State>) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.restaurantMenu.currentValue) {
      this.menuOptions = [{
        value: 'all',
        label: 'Все меню'
      }].concat(Object.keys(this.restaurantMenu).map(elem => ({
        value: elem,
        label: elem
      })));
    }
  }

  finish() {
    this.router.navigate(['booking', 'submit']);
  }

  cancel() {
    this.store.dispatch(new bookingActions.ClearPreorderFromMenu());
    this.router.navigate(['booking', 'submit']);
  }
}
