import { Component, Input } from '@angular/core';
import { MenuItem } from '../../../model/models';
import { State } from '../../../../state/app.state';
import { Store } from '@ngrx/store';
import * as fromBooking from '../../../../booking/state/booking.actions';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent {
  @Input() menuItem: MenuItem;
  @Input() isBooking: boolean;
  @Input() type: string;

  constructor(private store: Store<State>) {
  }

  increaseCount() {
    this.store.dispatch(new fromBooking.SetMenuItemCount({
      count: this.menuItem.count + 1,
      itemId: this.menuItem.uid,
      type: this.type
    }));
  }

  decreaseCount() {
    if (this.menuItem.count) {
      this.store.dispatch(new fromBooking.SetMenuItemCount({
        count: this.menuItem.count - 1,
        itemId: this.menuItem.uid,
        type: this.type
      }));
    }
  }
}
