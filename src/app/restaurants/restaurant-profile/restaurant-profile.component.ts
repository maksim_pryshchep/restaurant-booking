import { Component, Input, OnInit } from '@angular/core';
import { Restaurant } from '../model/models';
import { State } from '../../state/app.state';
import { Store } from '@ngrx/store';
import * as restaurantsActions from '../state/restaurants.actions';

@Component({
  selector: 'app-restaurant-profile',
  templateUrl: './restaurant-profile.component.html',
  styleUrls: ['./restaurant-profile.component.scss']
})
export class RestaurantProfileComponent implements OnInit {

  @Input() currentRestaurantProfile: Restaurant;
  @Input() isAuthenticated: boolean;
  @Input() isFavoriteRestaurant: boolean;

  constructor(private store: Store<State>) { }

  ngOnInit() {
  }

  getTitleImage(path: string) {
    return `url(${path})`;
  }

  onFavoriteAdd() {
    this.store.dispatch(new restaurantsActions.AddToFavorites());
  }
}
