import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { State } from '../../state/app.state';
import * as restaurantsActions from '../state/restaurants.actions';
import { getCurrentRestaurantProfile } from '../state/restaurants.selectors';
import * as fromBooking from '../../booking/state/booking.actions';
import { isAuthenticated, isFavoriteRestaurant } from '../../auth/state/auth.selectors';

@Component({
  selector: 'app-restaurant-profile-container',
  templateUrl: './restaurant-profile.container.html',
  styleUrls: ['./restaurant-profile.container.scss']
})
export class RestaurantProfileContainer implements OnDestroy {

  currentRestaurantProfile$;
  isAuthenticated$;
  isFavoriteRestaurant$;

  constructor(private activatedRoute: ActivatedRoute, private store: Store<State>) {
    const uid = this.activatedRoute.snapshot.paramMap.get('id');
    this.store.dispatch(new restaurantsActions.LoadRestaurantProfile({
      uid
    }));
    this.currentRestaurantProfile$ = this.store.pipe(select(getCurrentRestaurantProfile));
    this.isAuthenticated$ = this.store.pipe(select(isAuthenticated));
    this.isFavoriteRestaurant$ = this.store.pipe(select(isFavoriteRestaurant));
  }

  ngOnDestroy(): void {
  }

  startBooking() {
    this.store.dispatch(new fromBooking.StartBooking());
  }

}
