import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { State } from '../../../state/app.state';
import { getCurrentRestaurantProfile } from '../../state/restaurants.selectors';

@Component({
  selector: 'app-gallery-container',
  templateUrl: './gallery.container.html',
  styleUrls: ['./gallery.container.scss']
})
export class GalleryContainer {
  currentRestaurantProfile$;

  constructor(private store: Store<State>) {
    this.currentRestaurantProfile$ = this.store.pipe(select(getCurrentRestaurantProfile));
  }

}
