import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { State } from '../../../state/app.state';
import * as restaurantsActions from '../../state/restaurants.actions';
import { getCurrentUser, isAuthenticated } from '../../../auth/state/auth.selectors';
import { getReviews } from '../../state/restaurants.selectors';

@Component({
  selector: 'app-reviews-container',
  templateUrl: './reviews.container.html',
  styleUrls: ['./reviews.container.scss']
})
export class ReviewsContainer {

  user$;
  reviews$;

  constructor(private activatedRoute: ActivatedRoute, private store: Store<State>) {
    this.store.dispatch(new restaurantsActions.LoadReview());
    this.user$  = this.store.pipe(select(getCurrentUser));
    this.reviews$ = this.store.pipe(select(getReviews));
  }

}
