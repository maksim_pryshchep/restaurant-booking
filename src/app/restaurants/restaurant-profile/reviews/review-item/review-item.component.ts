import { Component, Input } from '@angular/core';
import { Review } from '../../../model/models';
import { firestore } from 'firebase';
import { State } from '../../../../state/app.state';
import { Store } from '@ngrx/store';
import * as restaurantsAction from '../../../state/restaurants.actions';
import { User } from '../../../../auth/model/models';

@Component({
  selector: 'app-review-item',
  templateUrl: './review-item.component.html',
  styleUrls: ['./review-item.component.scss']
})
export class ReviewItemComponent {

  @Input() review: Review;
  @Input() user: User;

  time = new Date();

  constructor(private store: Store<State>) {
  }

  isLike() {
    return this.user ? this.review.likes.includes(this.user.uid) : false;
  }

  isDislike() {
    return this.user ? this.review.dislikes.includes(this.user.uid) : false;
  }

  like() {
    if (this.user) {
      this.isLike() ? this.store.dispatch(new restaurantsAction.RemoveLike(this.review.uid)) :
        this.store.dispatch(new restaurantsAction.AddLike(this.review.uid));
      if (this.isDislike()) {
        this.store.dispatch(new restaurantsAction.RemoveDislike(this.review.uid))
      }
    }
  }

  dislike() {
    if (this.user) {
      this.isDislike() ? this.store.dispatch(new restaurantsAction.RemoveDislike(this.review.uid)) :
        this.store.dispatch(new restaurantsAction.AddDislike(this.review.uid));
      if (this.isLike()) {
        this.store.dispatch(new restaurantsAction.RemoveLike(this.review.uid))
      }
    }
  }

  getDate() {
    return this.review.date.toDate();
  }

  getIconString() {
    let res = '';
    this.review.displayName.split(' ').forEach(elem => {
      res += elem.charAt(0);
    });
    return res;

  }


}
