import { Component, Input } from '@angular/core';
import { State } from '../../../state/app.state';
import { Store } from '@ngrx/store';
import * as restaurantsActions from '../../state/restaurants.actions';
import { Review } from '../../model/models';
import { User } from '../../../auth/model/models';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent {

  @Input() user: User;
  @Input() reviews: Review[] = [];

  inputValue = '';

  submitting = false;

  constructor(private store: Store<State>) {
  }

  handleSubmit() {
    this.store.dispatch(new restaurantsActions.AppendReview(this.inputValue));
    this.inputValue = '';
  }

  getIconString() {
    let res = '';
    this.user.displayName.split(' ').forEach(elem => {
      res += elem.charAt(0);
    });
    return res;

  }

}
