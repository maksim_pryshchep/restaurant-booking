import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { State } from '../../../state/app.state';
import * as restaurantsActions from '../../state/restaurants.actions';
import { getCurrentRestaurantProfile } from '../../state/restaurants.selectors';

@Component({
  selector: 'app-main-container',
  templateUrl: './main.container.html',
  styleUrls: ['./main.container.scss']
})
export class MainContainer {

  currentRestaurantProfile$;

  constructor(private store: Store<State>) {
    this.currentRestaurantProfile$ = this.store.pipe(select(getCurrentRestaurantProfile));
  }

}
