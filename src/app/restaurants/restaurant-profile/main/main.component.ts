import { Component, Input, OnInit } from '@angular/core';
import { Restaurant } from '../../model/models';
import { NgxGalleryImage, NgxGalleryOptions } from '@kolkov/ngx-gallery';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  @Input() currentRestaurantProfile: Restaurant;

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  zoom = 14;

  constructor() {
  }

  ngOnInit(): void {
    this.galleryOptions = [

      { image: false, height: '100px' },
      { breakpoint: 500, width: '100%' }
    ];
    this.galleryImages = this.currentRestaurantProfile.galleryUrls.slice(0, 4).map(x => {
      return {
        small: x,
        medium: x,
        big: x
      };
    });
  }

}
