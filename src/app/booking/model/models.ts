import * as firebase from 'firebase';
import { ByKeyList, MenuItem } from '../../restaurants/model/models';

export interface BookingParameters {
  date: Date;
  timeFrom: Date;
  timeUntil: Date;
  numberOfGuests: number;
}

export interface ReservationRecord {
  uid?: string;
  userId: string;
  timeUntil: firebase.firestore.Timestamp;
  timeFrom: firebase.firestore.Timestamp;
  tableId: string;
  tableNumber: number;
  displayName: string;
  numberOfGuests: number;
  menu: ByKeyList<MenuItem[]>;
  approved: boolean;
  restaurantInfo: {
    restaurantId: string;
    title: string;
    titleImgUrl: string;
  };
  userOrderId?: string;
}
