import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducer } from './state/booking.reducer';
import { BookingEffects } from './state/booking.effects';
import { BookingSchemaComponent } from './booking-schema/booking-schema.component';
import {
  NzButtonModule,
  NzDatePickerModule,
  NzFormModule,
  NzModalModule,
  NzPopoverModule,
  NzSelectModule,
  NzTimePickerModule
} from 'ng-zorro-antd';
import { BookingParametersComponent } from './booking-parameters/booking-parameters.component';
import { SubmitBookingComponent } from './submit-booking/submit-booking.component';
import { BookingRouterModule } from './booking-router.module';
import { BookingParametersContainer } from './booking-parameters/booking-parameters.container';
import { BookingSchemaContainer } from './booking-schema/booking-schema.container';
import { SubmitBookingContainer } from './submit-booking/submit-booking.container';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    NzButtonModule,
    NzFormModule,
    StoreModule.forFeature('booking', reducer),
    EffectsModule.forFeature([BookingEffects]),
    NzSelectModule,
    NzDatePickerModule,
    NzTimePickerModule,
    BookingRouterModule,
    NzPopoverModule,
    NzModalModule
  ],
  declarations: [
    BookingSchemaComponent,
    BookingParametersComponent,
    SubmitBookingComponent,
    BookingParametersContainer,
    BookingSchemaContainer,
    SubmitBookingContainer
  ],
  entryComponents: [BookingParametersComponent]
})
export class BookingModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons();
  }
}
