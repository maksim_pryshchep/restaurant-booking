import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { State } from '../../state/app.state';
import { getBookingParameters } from '../state/booking.selector';

@Component({
  selector: 'app-booking-parameters-container',
  templateUrl: './booking-parameters.container.html',
  styleUrls: ['./booking-parameters.container.scss']
})
export class BookingParametersContainer {

  bookingParameters$;

  constructor(private store: Store<State>) {
    this.bookingParameters$ = this.store.pipe(select(getBookingParameters));
  }
}
