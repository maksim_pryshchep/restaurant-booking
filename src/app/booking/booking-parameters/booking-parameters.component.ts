import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BookingParameters } from '../model/models';
import { State } from '../../state/app.state';
import { Store } from '@ngrx/store';
import * as fromBooking from '../state/booking.actions';

@Component({
  selector: 'app-booking-parameters',
  templateUrl: './booking-parameters.component.html',
  styleUrls: ['./booking-parameters.component.scss']
})
export class BookingParametersComponent implements OnInit {
  @Input() bookingParameters: BookingParameters;

  bookingForm: FormGroup;

  constructor(private fb: FormBuilder, private state: Store<State>) {
  }

  ngOnInit(): void {
    this.bookingForm = this.fb.group({
      numberOfGuests: [this.bookingParameters.numberOfGuests.toString(), [Validators.required]],
      date: [this.bookingParameters.date, [Validators.required]],
      timeFrom: [this.bookingParameters.timeFrom, [Validators.required]],
      timeUntil: [new Date(), [Validators.required]]
    });
  }

  submitForm(): void {
    for (const i in this.bookingForm.controls) {
      this.bookingForm.controls[i].markAsDirty();
      this.bookingForm.controls[i].updateValueAndValidity();
    }
    if (this.bookingForm.valid) {
      this.state.dispatch(new fromBooking.SetBookingParameters(this.bookingForm.value));
    }
    console.log(this.bookingForm.value);
  }
}
