import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookingSchemaComponent } from './booking-schema/booking-schema.component';
import { SubmitBookingComponent } from './submit-booking/submit-booking.component';
import { BookingSchemaContainer } from './booking-schema/booking-schema.container';
import { SubmitBookingContainer } from './submit-booking/submit-booking.container';
import { MenuContainer } from '../restaurants/restaurant-profile/menu/menu.container';

const bookingRoutesChildRoutes: Routes = [
  { path: '', redirectTo: 'schema', pathMatch: 'full' },
  { path: 'schema', component: BookingSchemaContainer },
  { path: 'submit', component: SubmitBookingContainer },
  { path: 'menu', component: MenuContainer }
];

const bookingRoutes: Routes = [
  { path: 'booking', children: bookingRoutesChildRoutes }
];

@NgModule({
  imports: [
    RouterModule.forChild(bookingRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class BookingRouterModule {
}
