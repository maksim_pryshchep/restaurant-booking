import { Component } from '@angular/core';
import { State } from '../../state/app.state';
import { Store } from '@ngrx/store';
import { getReservationRecord } from '../state/booking.selector';
import * as fromBooking from '../state/booking.actions';

@Component({
  selector: 'app-submit-booking-container',
  templateUrl: './submit-booking.container.html',
  styleUrls: ['./submit-booking.container.scss']
})
export class SubmitBookingContainer {

  reservationRecord$;

  constructor(private store: Store<State>) {
    this.reservationRecord$ = this.store.select(getReservationRecord);
  }
}
