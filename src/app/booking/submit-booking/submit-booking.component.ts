import { Component, Input } from '@angular/core';
import { ReservationRecord } from '../model/models';
import { firestore } from 'firebase';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import * as bookingActions from '../state/booking.actions';
import { Store } from '@ngrx/store';
import { State } from '../../state/app.state';

@Component({
  selector: 'app-submit-booking',
  templateUrl: './submit-booking.component.html',
  styleUrls: ['./submit-booking.component.scss']
})
export class SubmitBookingComponent {
  @Input() reservationRecord: ReservationRecord;

  constructor(private location: Location, private router: Router, private store: Store<State>) {
  }

  getDate(date: firestore.Timestamp) {
    return date.toDate();
  }

  goBack() {
    this.location.back();
  }

  showMenu() {
    this.router.navigate(['booking', 'menu'], {
      queryParams: {
        isBooking: true
      },
      skipLocationChange: true
    });
  }

  submitBooking() {
    this.store.dispatch(new bookingActions.SubmitReservation());
  }
}
