import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import * as d3 from 'd3';
import { ByKeyList, Restaurant } from '../../restaurants/model/models';
import { Store } from '@ngrx/store';
import { State } from '../../state/app.state';
import * as bookingActions from '../state/booking.actions';

@Component({
  selector: 'app-booking-schema',
  templateUrl: './booking-schema.component.html',
  styleUrls: ['./booking-schema.component.scss']
})
export class BookingSchemaComponent implements AfterViewInit, OnInit, OnChanges {
  @ViewChild('svgObject') object: ElementRef;
  @Input() currentRestaurantProfile: Restaurant;
  @Input() tableParameters: ByKeyList<{ available: boolean; placesCount: number, tableNumber: number }>;

  tables: ByKeyList<{ available: boolean; placesCount: number, tableNumber: number }> = {};

  showBookingParameters: boolean;

  constructor(private store: Store<State>) {
  }

  ngOnInit() {
    if (!this.tableParameters) {
      this.currentRestaurantProfile.tables.forEach(elem => {
        this.tables[elem.uid] = {
          available: null,
          placesCount: elem.placesCount,
          tableNumber: elem.tableNumber
        };
      });
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.tableParameters && changes.tableParameters.currentValue) {
      this.tables = this.tableParameters;
      this.paintTables();
    }
  }

  ngAfterViewInit(): void {
    d3.xml(this.currentRestaurantProfile.schemaUrl).then(data => {
      this.object.nativeElement.appendChild(data.documentElement);
      const zoom = d3.zoom();
      const svg = d3.select('#schema');

      // @ts-ignore
      const width = +d3.select('#schema').select('g').node().getBoundingClientRect().width;

      svg.attr('pointer-events', 'all').call(zoom.on('zoom', () => {
        svg.select('g').attr('transform', d3.event.transform);
      })).call(zoom.transform, d3.zoomIdentity.translate(-(1200 - window.innerWidth / 2 + width / 2), -560));

      const clickHandler = (tableId: string) => {
        if (this.tables[tableId].available) {
          this.store.dispatch(new bookingActions.BuildReservationRecord({
            tableId,
            tableNumber: this.tables[tableId].tableNumber
          }));
        }
      };

      svg.selectAll('.table').each(function() {
        const a = d3.select(this);
        const tableId = a.attr('id');
        a.on('click', () => {
          clickHandler(tableId);
        });
      });

      if (this.tableParameters) {
        this.paintTables();
      }
    });
  }

  paintTables() {
    const svg = d3.select('#schema');
    const getTableData = (id: string) => {
      return this.tables[id];
    };

    svg.selectAll('.table').each(function() {
      const a = d3.select(this);
      const tableId = a.attr('id');
      const tableData = getTableData(tableId);
      if (tableData.available) {
        a.select('ellipse').attr('fill', '#3ec556');
      } else {
        a.select('ellipse').attr('fill', '#bcbcbc');
      }
    });
  }

  hideParameters() {
    if (this.showBookingParameters) {
      this.showBookingParameters = false;
    }
  }

  showParameters() {
    this.showBookingParameters = true;
  }
}
