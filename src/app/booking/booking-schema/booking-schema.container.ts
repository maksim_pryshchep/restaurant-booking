import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { State } from '../../state/app.state';
import { getCurrentRestaurantProfile } from '../../restaurants/state/restaurants.selectors';
import { getTablesParameters } from '../state/booking.selector';

@Component({
  selector: 'app-booking-schema-container',
  templateUrl: './booking-schema.container.html',
  styleUrls: ['./booking-schema.container.scss']
})
export class BookingSchemaContainer {
  currentRestaurantProfile$;
  tableParameters$;

  constructor(private activatedRoute: ActivatedRoute, private store: Store<State>) {
    this.currentRestaurantProfile$ = this.store.pipe(select(getCurrentRestaurantProfile));
    this.tableParameters$ = this.store.pipe(select(getTablesParameters));
  }

}
