import { createFeatureSelector, createSelector } from '@ngrx/store';
import { BookingState } from './booking.reducer';

const getBookingFeatureState = createFeatureSelector<BookingState>('booking');

export const getBookingParameters = createSelector(
  getBookingFeatureState,
  state => state.bookingParameters
);

export const getTablesParameters = createSelector(
  getBookingFeatureState,
  state => state.tableParameters
);

export const getReservationRecord = createSelector(
  getBookingFeatureState,
  state => state.reservationRecord
);

export const getMenuFromBookingParameters = createSelector(
  getBookingFeatureState,
  state => state.reservationRecord.menu
);
