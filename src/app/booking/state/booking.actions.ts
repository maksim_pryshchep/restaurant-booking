import { Action } from '@ngrx/store';
import { BookingParameters, ReservationRecord } from '../model/models';
import { ByKeyList, MenuItem } from '../../restaurants/model/models';

export enum BookingActionTypes {
  StartBooking = '[Booking] Start Booking',
  SetBookingParameters = '[Booking] Set Booking Parameters',
  StoreNewBookingParameters = '[Booking] Store Booking Parameters',
  StoreTableParameters = '[Booking] Store Table Parameters',
  BuildReservationRecord = '[Booking] Build Reservation Record',
  StoreReservationRecord = '[Booking] Store Reservation Record',
  ClearPreorderFromMenu = '[Booking] Clear Preorder From Menu',
  SetMenuItemCount = '[Booking] Set Menu Item Count',
  SubmitReservation = '[Booking] Submit Reservation'
}

export class SubmitReservation {
  readonly type = BookingActionTypes.SubmitReservation;
}

export class ClearPreorderFromMenu {
  readonly type = BookingActionTypes.ClearPreorderFromMenu;
}

export class SetMenuItemCount implements Action {
  readonly type = BookingActionTypes.SetMenuItemCount;

  constructor(public payload: {itemId: string, count: number, type: string}) {
  }
}

export class StoreReservationRecord implements Action {
  readonly type = BookingActionTypes.StoreReservationRecord;

  constructor(public payload: ReservationRecord) {
  }
}

export class BuildReservationRecord implements Action {
  readonly type = BookingActionTypes.BuildReservationRecord;

  constructor(public payload: { tableId: string, tableNumber: number }) {
  }
}

export class StoreTableParameters implements Action {
  readonly type = BookingActionTypes.StoreTableParameters;

  constructor(public payload: ByKeyList<{ available: boolean; placesCount: number }>) {
  }
}

export class StartBooking implements Action {
  readonly type = BookingActionTypes.StartBooking;
}

export class SetBookingParameters implements Action {
  readonly type = BookingActionTypes.SetBookingParameters;

  constructor(public payload: BookingParameters) {
  }

}

export class StoreNewBookingParameters implements Action {
  readonly type = BookingActionTypes.StoreNewBookingParameters;

  constructor(public payload: BookingParameters) {
  }

}


export type BookingActions = StartBooking |
  SetBookingParameters |
  StoreNewBookingParameters |
  StoreTableParameters |
  BuildReservationRecord |
  StoreReservationRecord |
  SetMenuItemCount |
  ClearPreorderFromMenu |
  SubmitReservation;
