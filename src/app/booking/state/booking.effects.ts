import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as bookingAction from './booking.actions';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { State } from '../../state/app.state';
import { getCurrentUser, isAuthenticated } from '../../auth/state/auth.selectors';
import { NzModalService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { toPayload } from '../../auth/state/auth.effects';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { ReservationRecord } from '../model/models';
import { ByKeyList, MenuItem } from '../../restaurants/model/models';
import { getCurrentRestaurantProfile } from '../../restaurants/state/restaurants.selectors';
import { getBookingParameters, getReservationRecord } from './booking.selector';
import { from } from 'rxjs';

@Injectable()
export class BookingEffects {
  constructor(private actions$: Actions,
              private store$: Store<State>,
              private modalService: NzModalService,
              private afs: AngularFirestore,
              private modal: NzModalService,
              private router: Router) {

  }

  @Effect({ dispatch: false }) startBooking$ = this.actions$.pipe(
    ofType<bookingAction.StartBooking>(bookingAction.BookingActionTypes.StartBooking),
    withLatestFrom(this.store$.pipe(select(isAuthenticated)), (x, isAuth) => ({ x, isAuth })),
    tap(x => {
      if (!x.isAuth) {
        this.modalService.info({
          nzTitle: 'Внимание!',
          nzContent: '<p>Чтобы продолжить работу, необходимо войти в систему!</p>',
          nzKeyboard: true,
          nzMaskClosable: true
        });
      } else {
        this.router.navigate(['booking']);
      }
    })
  );

  @Effect() setBookingParameters$ = this.actions$.pipe(
    ofType<bookingAction.SetBookingParameters>(bookingAction.BookingActionTypes.SetBookingParameters),
    map(toPayload),
    map(x => {
      x.date.setHours(0);
      x.date.setMinutes(0);
      x.date.setSeconds(0);
      x.date.setMilliseconds(0);
      x.timeFrom.setDate(x.date.getDate());
      x.timeFrom.setMonth(x.date.getMonth());
      x.timeFrom.setSeconds(0);
      x.timeFrom.setMilliseconds(0);

      x.timeUntil.setDate(x.date.getDate());
      x.timeUntil.setMonth(x.date.getMonth());
      x.timeUntil.setSeconds(0);
      x.timeUntil.setMilliseconds(0);

      x.numberOfGuests = +x.numberOfGuests;
      return new bookingAction.StoreNewBookingParameters(x);
    }));

  @Effect({ dispatch: false }) getSchemaParameters$ = this.actions$.pipe(
    ofType<bookingAction.StoreNewBookingParameters>(bookingAction.BookingActionTypes.StoreNewBookingParameters),
    map(toPayload),
    withLatestFrom(
      this.store$.select(getCurrentRestaurantProfile),
      (payload, restaurantProfile) => ({
        payload,
        restaurantId: restaurantProfile.uid
      })
    ),
    switchMap(x => {
      return this.afs.collection<ReservationRecord>('restaurants').doc(x.restaurantId).collection('reservations').valueChanges().pipe(
        map((y: ReservationRecord[]) => {
          return y.filter(elem => !(x.payload.timeUntil.valueOf() <= elem.timeFrom.toMillis() && x.payload.timeFrom.valueOf() <= elem.timeFrom.toMillis()) &&
            !(x.payload.timeUntil.valueOf() >= elem.timeUntil.toMillis() && x.payload.timeFrom.valueOf() >= elem.timeUntil.toMillis()));
        }),
        map(y => {
          const notAvailableTables: ByKeyList<{ available: boolean; }> = {};
          y.forEach(elem => {
            notAvailableTables[elem.tableId] = {
              available: false
            };
          });
          return notAvailableTables;
        })
      );
    }),
  );

  @Effect() prepareSchemaParameters$ = this.getSchemaParameters$.pipe(
    withLatestFrom(
      this.store$.select(getCurrentRestaurantProfile),
      this.store$.select(getBookingParameters),
      (payload, restaurantProfile, bookingParameters) => ({
        notAvailableRestaurants: payload,
        tableList: restaurantProfile.tables,
        bookingParameters
      })
    ),
    map(x => {
      const parameters: ByKeyList<{ available: boolean; placesCount: number, tableNumber: number }> = {};
      x.tableList.forEach(elem => {
        const condition = elem.placesCount >= x.bookingParameters.numberOfGuests &&
          elem.placesCount - x.bookingParameters.numberOfGuests <= 2;
        parameters[elem.uid] = {
          placesCount: elem.placesCount,
          available: x.notAvailableRestaurants[elem.uid] ? x.notAvailableRestaurants[elem.uid].available && condition : condition,
          tableNumber: elem.tableNumber
        };
      });
      return new bookingAction.StoreTableParameters(parameters);
    })
  );

  @Effect() buildReservationRecord$ = this.actions$.pipe(
    ofType<bookingAction.BuildReservationRecord>(bookingAction.BookingActionTypes.BuildReservationRecord),
    map(toPayload),
    withLatestFrom(
      this.store$.select(getCurrentRestaurantProfile),
      this.store$.select(getBookingParameters),
      this.store$.select(getCurrentUser),
      (payload, restaurantProfile, bookingParameters, currentUser) => ({
        payload,
        restaurantProfile,
        bookingParameters,
        currentUser
      })
    ),
    switchMap(x => {
      return this.afs.collection<{ type: string; items: MenuItem[] }>('restaurants').doc(x.restaurantProfile.uid).collection('menu')
        .valueChanges().pipe(
          map(types => {
            const menu = {};
            types.forEach(elem => {
              menu[elem.type] = elem.items.map(elem => {
                elem.count = 0;
                return elem;
              });
            });
            return menu;
          }),
          map(menu => {
            return new bookingAction.StoreReservationRecord({
              menu,
              userId: x.currentUser.uid,
              timeUntil: firebase.firestore.Timestamp.fromDate(x.bookingParameters.timeUntil),
              timeFrom: firebase.firestore.Timestamp.fromDate(x.bookingParameters.timeFrom),
              tableId: x.payload.tableId,
              tableNumber: x.payload.tableNumber,
              displayName: x.currentUser.displayName,
              numberOfGuests: x.bookingParameters.numberOfGuests,
              approved: false,
              restaurantInfo: {
                restaurantId: x.restaurantProfile.uid,
                title: x.restaurantProfile.name,
                titleImgUrl: x.restaurantProfile.titleImageUrl
              }
            });
          })
        );
    }),
  );

  @Effect({ dispatch: false }) navigateToSubmitBookingComponent$ = this.actions$.pipe(
    ofType<bookingAction.StoreReservationRecord>(bookingAction.BookingActionTypes.StoreReservationRecord),
    tap(x => {
      this.router.navigate(['booking', 'submit']);
    })
  );

  @Effect({ dispatch: false }) submitReservation$ = this.actions$.pipe(
    ofType<bookingAction.SubmitReservation>(bookingAction.BookingActionTypes.SubmitReservation),
    withLatestFrom(
      this.store$.select(getCurrentRestaurantProfile),
      this.store$.select(getReservationRecord),
      this.store$.select(getCurrentUser),
      (payload, restaurant, reservationRecord, user) => ({
        restaurant,
        reservationRecord,
        user
      })
    ),
    switchMap(x => {
      return from(this.afs.collection('users').doc(x.user.uid).collection('orders').add(x.reservationRecord)).pipe(
          switchMap(userOrder => {
            return from(this.afs.collection('restaurants').doc(x.restaurant.uid).collection('reservations').add({
              ...x.reservationRecord,
              userOrderId: userOrder.id
            })).pipe(
              map(restaurantReservation => {
                setTimeout(() => {
                  this.modal.success({
                    nzTitle: 'Ваш заказ успесно создан!',
                    nzContent: 'Подтверждение заказа будет выслано на вашу электронную почту.'
                  });
                }, 0);
                this.router.navigate(['']);
              })
            )
          })
      );
    })
  );

}
