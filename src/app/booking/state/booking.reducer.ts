import { BookingActions, BookingActionTypes } from './booking.actions';
import { BookingParameters, ReservationRecord } from '../model/models';
import { ByKeyList } from '../../restaurants/model/models';

export interface BookingState {
  bookingParameters: BookingParameters;
  tableParameters: ByKeyList<{ available: boolean; placesCount: number }>;
  reservationRecord: ReservationRecord;
}

export function reducer(state: BookingState = initialState, action: BookingActions): BookingState {
  switch (action.type) {
    case BookingActionTypes.ClearPreorderFromMenu: {
      const reservationRecord = state.reservationRecord;
      Object.keys(reservationRecord.menu).forEach(index => {
        reservationRecord.menu[index] = reservationRecord.menu[index].map(elem => ({ ...elem, count: 0 }));
      });
      return {
        ...state,
        reservationRecord
      };
    }
    case BookingActionTypes.SetMenuItemCount: {
      const reservationRecord = {...state.reservationRecord};
      reservationRecord.menu[action.payload.type] = reservationRecord.menu[action.payload.type].map(elem => {
        if (elem.uid === action.payload.itemId) {
          elem.count = action.payload.count;
        }
        return elem;
      });
      return {
        ...state,
        reservationRecord
      };
    }
    case BookingActionTypes.StoreReservationRecord: {
      return {
        ...state,
        reservationRecord: action.payload
      };
    }
    case BookingActionTypes.StoreTableParameters:
      return {
        ...state,
        tableParameters: action.payload
      };
    case BookingActionTypes.StoreNewBookingParameters:
      return {
        ...state,
        bookingParameters: action.payload
      };
    default:
      return {
        ...state
      };
  }
}

const initialState: BookingState = {
  bookingParameters: {
    numberOfGuests: 5,
    date: new Date(),
    timeFrom: new Date(),
    timeUntil: null,
  },
  tableParameters: null,
  reservationRecord: null
};
