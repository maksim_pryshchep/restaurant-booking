import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { isBookingPage, showHeaderFooter } from './router-store/reducers';
import { Router } from '@angular/router';
import { State } from './state/app.state';
import * as fromAuth from './auth/state/auth.actions';
import { getDisplayNameValue, isAuthenticated, isLoadingUser } from './auth/state/auth.selectors';
import { isLoadingRestaurants } from './restaurants/state/restaurants.selectors';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  showHeaderFooter$;
  isAuthenticated$;
  displayName$;
  isLoadingUser$;
  isLoadingRestaurants$;
  isBookingPage$;
  showPopup: boolean;

  constructor(private state: Store<State>,
              private router: Router) {
    this.showHeaderFooter$ = this.state.pipe(select(showHeaderFooter));
    this.isAuthenticated$ = this.state.pipe(select(isAuthenticated));
    this.isBookingPage$ = this.state.pipe(select(isBookingPage));
    this.displayName$ = this.state.pipe(select(getDisplayNameValue));
    this.isLoadingUser$ = this.state.pipe(select(isLoadingUser));
    this.isLoadingRestaurants$ = this.state.pipe(
      delay(0), select(isLoadingRestaurants));
    this.state.dispatch(new fromAuth.GetUser());
  }

  visible: boolean;

  hidePopup(): void {
    this.showPopup = false;
  }

  navigateToLoginPage() {
    this.router.navigate(['/auth/login']);
  }

  logout() {
    this.state.dispatch(new fromAuth.SignOut());
  }
}
