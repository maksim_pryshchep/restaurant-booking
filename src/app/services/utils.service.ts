import { Injectable } from '@angular/core';
import { BaseRating } from '../restaurants/model/models';

@Injectable()
export class UtilsService {
    constructor() {

    }

    getStars(rating: BaseRating) {
        const result = []; 
        let starCount = 5;
        let averageMark = Object.values(rating).reduce((a,b) => a + b, 0) / Object.values(rating).length;
        while (averageMark >= 1) {
            result.push('star-full');
            averageMark--;
            starCount--;
        }

        if (averageMark >= 0.875) {
            result.push('star-full');
        } else if (averageMark >= 0.75) {
            result.push('star-threequarter');
        } else if (averageMark >= 0.625) {
            result.push('star-threequarter');
        } else if (averageMark >= 0.5) {
            result.push('star-half');
        } else if (averageMark >= 0.375) {
            result.push('star-half');
        } else if (averageMark >= 0.25) {
            result.push('star-quarter');
        } else if (averageMark >= 0.125) {
            result.push('star-quarter');
        } else {
            result.push('star-empty');
        }

        while (--starCount) {
            result.push('star-empty');
        }

        return result;
    }
}