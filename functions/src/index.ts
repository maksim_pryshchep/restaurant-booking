import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import  * as nodemailer from 'nodemailer';
const hbs = require("nodemailer-express-handlebars");
admin.initializeApp();


const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  auth: {
    user: functions.config().gmail.email,
    pass: functions.config().gmail.password
  }
});

const templatesFolder = __dirname + "/templates";
transporter.use(
  "compile",
  hbs({
    viewPath: templatesFolder,
    extName: ".handlebars",
    viewEngine: {
      extName: '.handlebars',
      partialsDir: templatesFolder,
      layoutsDir: templatesFolder,
      defaultLayout: 'index.handlebars',
    },
  })
);

export const usersCreate = functions.firestore
    .document('users/{userId}')
    .onCreate(async (snap, context) => {
        const user = snap.data();
        if (user && user.provider === 'facebook.com') {
          await snap.ref.set({
            emailVerified: true,
          }, { merge: true });
          return admin.auth().updateUser(user.uid, {
            emailVerified: true,
          });
        }

      return null;
    });

export const approveOrder = functions.firestore.document('restaurants/{restaurantId}/reservations/{reservationId}')
  .onUpdate(async(snap, context) => {
    const order = snap.after.data();
    if (order) {
      const userId = order.userId;
      const user = await admin.firestore().collection('users').doc(userId).get();
      const userData = user.data();
      if (user && userData) {
        await user.ref.collection('orders').doc(order.userOrderId).update({
          approved: order.approved
        });
        const orderDate = order.timeFrom.toDate();
        const mailOptions = {
          from: 'Get Table <pryshchep05091999@gmail.com>',
          to: userData.email,
          subject: 'Подтверждение заказа',
          template: 'index',
          attachments: [{
            filename: 'booking.png',
            path: __dirname +'/booking.png',
            cid: 'booking'
          }],
          context: {
            numberOfGuests: order.numberOfGuests,
            date: `${orderDate.getDate()}.${orderDate.getMonth() + 1}.${orderDate.getFullYear()}`,
            time: `${orderDate.getHours() + 3}:${orderDate.getMinutes()}`,
            tableNumber: order.tableNumber
          }
        };
        try {
          await transporter.sendMail(mailOptions)
          console.log("Email sent to:", userData.email);
        } catch (error) {
          console.error("Error sending email:", error);
        }

      }
    }
    return null;
  });
